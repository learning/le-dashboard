import React from 'react'
import { DashboardPie } from '../components'
import { Row, Col } from 'antd'
import DummyData from '../lib/DummyData'

class VESPage extends React.Component {

  render () {
    return (
      <Row>
        <Col xs={24} sm={24} md={24} lg={24} xl={12}>
          <DashboardPie
            header='Interactions by Project'
            // data={this.state.data}
            data={DummyData.pie}
          />
        </Col>
        <Col xs={24} sm={24} md={24} lg={24} xl={12}>
          <DashboardPie
            header='Project Stats'
            // data={this.state.data}
            data={DummyData.pie}
          />
        </Col>
      </Row>
    )
  }
}

export const VES = VESPage
