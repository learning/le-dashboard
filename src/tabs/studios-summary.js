import React from 'react'
import { DashboardPie, DashboardLine, DashboardBar, DashboardCalendar, DashboardRadar, DashboardTreemap } from '../components'
import { Row, Col } from 'antd'
import DummyData from '../lib/DummyData'

class StudiosSummaryPage extends React.Component {

  render () {
    return (
      <div>
        <Row>
          <Col xs={24} sm={24} md={24} lg={24} xl={12}>
            <DashboardPie
              header='Interactions by Project'
              // data={this.state.data}
              data={DummyData.pie}
            />
          </Col>
          <Col xs={24} sm={24} md={24} lg={24} xl={12}>
            <DashboardPie
              header='Project Stats'
              // data={this.state.data}
              data={DummyData.pie}
            />
          </Col>
        </Row>
        <Row>
          <Col xs={24} sm={24} md={24} lg={24} xl={24}>
            <DashboardLine
              header='Hours of Use per Printer'
              // data={this.state.data}
              data={DummyData.line}
            />
          </Col>
        </Row>
        <Row>
          <Col xs={24} sm={24} md={24} lg={24} xl={24}>
            <DashboardBar
              header='Use over time'
              // data={this.state.data}
              data={DummyData.bar}
            />
          </Col>
        </Row>
        <Row>
          <Col xs={24} sm={24} md={24} lg={24} xl={12}>
            <DashboardCalendar
              header='Use per day'
              // data={this.state.data}
              data={DummyData.calendar}
              from="2015-03-01"
              to="2016-07-12"
            />
          </Col>
          <Col xs={24} sm={24} md={24} lg={24} xl={12}>
            <DashboardRadar
              header='Studios by Values'
              // data={this.state.data}
              data={DummyData.radar}
            />
          </Col>
        </Row>
        <Row>
          <Col xs={24} sm={24} md={24} lg={24} xl={12}>
            <DashboardTreemap
              header='Use by Department'
              // data={this.state.data}
              data={DummyData.treemap}
            />
          </Col>
        </Row>
      </div>
    )
  }
}

export const StudiosSummary = StudiosSummaryPage
