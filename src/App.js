import * as React from 'react'
import './app.css'
import { Layout, Tabs } from 'antd'
import { StudiosSummary, DDS, VES, FS, MDS, Exhibits, Outreach } from './tabs'
const { Header, Content } = Layout
const TabPane = Tabs.TabPane

class App extends React.Component {

  constructor (props) {
    super(props)

    this._async = this._async.bind(this)

    this.state = {
      data: []
    }
  }

  componentWillMount () {
    // this._async()
  }

  _async = () => {
    fetch('../data/pie')
    .then((response) => {
      return response.json()
    })
    .then((response) => {
      this.setState({data: response})
    })
  }

  render () {
    console.log(<StudiosSummary></StudiosSummary>)
    return (
      <Layout>
        <Header style={{height: 105}}>
          <div className="logo">
            <img src='assets/logo.svg' alt="Logo for the Virginia Tech University Libraries" />
          </div>
          <div className="site-title">
            <h1>Learning Environments</h1>
          </div>
        </Header>
        <Content>
          <Tabs tabPosition={this.state.tabPosition}>
            <TabPane tab="Studios Summary" key="1">
              <StudiosSummary />
            </TabPane>
            <TabPane tab="Virtual Environments Studio" key="2">
              <VES />
            </TabPane>
            <TabPane tab="3D Design Studio" key="3">
              <DDS />
            </TabPane>
            <TabPane tab="Fusion Studio" key="4">
              <FS />
            </TabPane>
            <TabPane tab="Media Design Studio" key="5">
              <MDS />
            </TabPane>
            <TabPane tab="Exhibits" key="6">
              <Exhibits />
            </TabPane>
            <TabPane tab="Outreach" key="7">
              <Outreach />
            </TabPane>
          </Tabs>
        </Content>
      </Layout>
    )
  }
}

export default App
