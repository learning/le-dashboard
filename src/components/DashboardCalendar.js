import * as React from 'react'
import { ResponsiveCalendar } from '@nivo/calendar'
import { CustomColors } from './common/variables'
import Heading from './common/Heading'
import './common/App.css'

const theme = {
  radialLabels: {
    textColor: '#000',
    fontSize: '50px'
  }
}

class CustomCalendar extends React.Component {
  render () {
    return (
      <div className='container'>
        <Heading
          header={this.props.header}
        />
        <div className='visualization'>
          <ResponsiveCalendar
            data={this.props.data}
            from={this.props.from}
            to={this.props.to}
            emptyColor="#eeeeee"
            colors={CustomColors}
            margin={{
                "top": 100,
                "right": 30,
                "bottom": 60,
                "left": 30
            }}
            yearSpacing={40}
            monthBorderColor="#ffffff"
            monthLegendOffset={10}
            dayBorderWidth={2}
            dayBorderColor="#ffffff"
            legends={[
              {
                  "anchor": "bottom-right",
                  "direction": "row",
                  "translateY": 36,
                  "itemCount": 4,
                  "itemWidth": 34,
                  "itemHeight": 36,
                  "itemDirection": "top-to-bottom"
              }
            ]}
          />
        </div>
      </div>
    )
  }
}

export const DashboardCalendar = CustomCalendar
