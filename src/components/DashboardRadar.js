import * as React from 'react'
import { ResponsiveRadar } from '@nivo/radar'
import { CustomColors } from './common/variables'
import './common/App.css'

const theme = {
  radialLabels: {
    textColor: '#000',
    fontSize: '50px'
  }
}

class CustomRadar extends React.Component {
  render () {
    return (
      <div className='container'>
        <div className='header'>
          {this.props.header}
        </div>
        <div className='visualization'>
          <ResponsiveRadar
            data={this.props.data}
            keys={[
                "chardonay",
                "carmenere",
                "syrah"
            ]}
            indexBy="taste"
            margin={{
                "top": 70,
                "right": 80,
                "bottom": 40,
                "left": 80
            }}
            curve="catmullRomClosed"
            borderWidth={2}
            borderColor="inherit"
            gridLevels={5}
            gridShape="circular"
            gridLabelOffset={36}
            enableDots={true}
            dotSize={8}
            dotColor="inherit"
            dotBorderWidth={0}
            dotBorderColor="#ffffff"
            enableDotLabel={true}
            dotLabel="value"
            dotLabelYOffset={-12}
            colors={CustomColors}
            colorBy="key"
            fillOpacity={0.1}
            animate={true}
            motionStiffness={90}
            motionDamping={15}
            isInteractive={true}
            legends={[
              {
                "anchor": "top-left",
                "direction": "column",
                "translateX": -50,
                "translateY": -40,
                "itemWidth": 80,
                "itemHeight": 20,
                "symbolSize": 12,
                "symbolShape": "circle"
              }
            ]}
          />
        </div>
      </div>
    )
  }
}

export const DashboardRadar = CustomRadar
