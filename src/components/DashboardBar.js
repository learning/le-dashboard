import * as React from 'react'
import { ResponsiveBar } from '@nivo/bar'
import { CustomColors } from './common/variables'
import Heading from './common/Heading'
import './common/App.css'

const theme = {
  radialLabels: {
    textColor: '#000',
    fontSize: '50px'
  }
}

class CustomBar extends React.Component {
  render () {
    return (
      <div className='container'>
        <Heading
          header={this.props.header}
        />
        <div className='visualization'>
          <ResponsiveBar
            data={this.props.data}
            keys={[
              'hot dog',
              'burger',
              'sandwich',
              'kebab',
              'fries',
              'donut'
            ]}
            indexBy='country'
            margin={{
              'top': 50,
              'right': 150,
              'bottom': 50,
              'left': 60
            }}
            padding={0.3}
            colors={CustomColors}
            colorBy='id'
            borderColor='inherit:darker(1.6)'
            axisBottom={{
              'orient': 'bottom',
              'tickSize': 5,
              'tickPadding': 5,
              'tickRotation': 0,
              'legend': 'country',
              'legendPosition': 'center',
              'legendOffset': 36
            }}
            axisLeft={{
              'orient': 'left',
              'tickSize': 5,
              'tickPadding': 5,
              'tickRotation': 0,
              'legend': 'food',
              'legendPosition': 'center',
              'legendOffset': -40
            }}
            labelSkipWidth={12}
            labelSkipHeight={12}
            labelTextColor='inherit:darker(1.6)'
            animate
            motionStiffness={90}
            motionDamping={15}
            legends={[
              {
                'dataFrom': 'keys',
                'anchor': 'bottom-right',
                'direction': 'column',
                'translateX': 120,
                'itemWidth': 100,
                'itemHeight': 20,
                'itemsSpacing': 2,
                'symbolSize': 20
              }
            ]}
          />
        </div>
      </div>
    )
  }
}

export const DashboardBar = CustomBar
