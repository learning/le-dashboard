module.exports = {
  pie: [{ 'id': 'Building MDS', 'label': 'Building MDS', 'value': 3 }, { 'id': 'Library Instruction Fall 2018', 'label': 'Library Instruction Fall 2018', 'value': 5 }, { 'id': 'Studio Tours Spring 2018', 'label': 'Studio Tours Spring 2018', 'value': 1 }, { 'id': 'ePortfolios Support Spring 2018', 'label': 'ePortfolios Support Spring 2018', 'value': 0 }],
  line: [
    {
      'id': 'whisky',
      'color': 'hsl(242, 70%, 50%)',
      'data': [
        {
          'color': 'hsl(7, 70%, 50%)',
          'x': 'PT',
          'y': 3
        },
        {
          'color': 'hsl(196, 70%, 50%)',
          'x': 'KW',
          'y': 17
        },
        {
          'color': 'hsl(318, 70%, 50%)',
          'x': 'MK',
          'y': 29
        },
        {
          'color': 'hsl(268, 70%, 50%)',
          'x': 'NG',
          'y': 8
        },
        {
          'color': 'hsl(294, 70%, 50%)',
          'x': 'FJ',
          'y': 28
        },
        {
          'color': 'hsl(198, 70%, 50%)',
          'x': 'TM',
          'y': 46
        },
        {
          'color': 'hsl(151, 70%, 50%)',
          'x': 'TJ',
          'y': 53
        },
        {
          'color': 'hsl(182, 70%, 50%)',
          'x': 'PM',
          'y': 14
        },
        {
          'color': 'hsl(6, 70%, 50%)',
          'x': 'BA',
          'y': 23
        }
      ]
    },
    {
      'id': 'rhum',
      'color': 'hsl(277, 70%, 50%)',
      'data': [
        {
          'color': 'hsl(7, 70%, 50%)',
          'x': 'PT',
          'y': 44
        },
        {
          'color': 'hsl(136, 70%, 50%)',
          'x': 'KW',
          'y': 39
        },
        {
          'color': 'hsl(79, 70%, 50%)',
          'x': 'MK',
          'y': 31
        },
        {
          'color': 'hsl(51, 70%, 50%)',
          'x': 'NG',
          'y': 34
        },
        {
          'color': 'hsl(264, 70%, 50%)',
          'x': 'FJ',
          'y': 23
        },
        {
          'color': 'hsl(252, 70%, 50%)',
          'x': 'TM',
          'y': 23
        },
        {
          'color': 'hsl(145, 70%, 50%)',
          'x': 'TJ',
          'y': 7
        },
        {
          'color': 'hsl(285, 70%, 50%)',
          'x': 'PM',
          'y': 57
        },
        {
          'color': 'hsl(301, 70%, 50%)',
          'x': 'BA',
          'y': 51
        }
      ]
    },
    {
      'id': 'gin',
      'color': 'hsl(203, 70%, 50%)',
      'data': [
        {
          'color': 'hsl(360, 70%, 50%)',
          'x': 'PT',
          'y': 19
        },
        {
          'color': 'hsl(274, 70%, 50%)',
          'x': 'KW',
          'y': 59
        },
        {
          'color': 'hsl(252, 70%, 50%)',
          'x': 'MK',
          'y': 42
        },
        {
          'color': 'hsl(332, 70%, 50%)',
          'x': 'NG',
          'y': 55
        },
        {
          'color': 'hsl(211, 70%, 50%)',
          'x': 'FJ',
          'y': 41
        },
        {
          'color': 'hsl(264, 70%, 50%)',
          'x': 'TM',
          'y': 30
        },
        {
          'color': 'hsl(312, 70%, 50%)',
          'x': 'TJ',
          'y': 59
        },
        {
          'color': 'hsl(305, 70%, 50%)',
          'x': 'PM',
          'y': 30
        },
        {
          'color': 'hsl(70, 70%, 50%)',
          'x': 'BA',
          'y': 53
        }
      ]
    },
    {
      'id': 'vodka',
      'color': 'hsl(311, 70%, 50%)',
      'data': [
        {
          'color': 'hsl(243, 70%, 50%)',
          'x': 'PT',
          'y': 35
        },
        {
          'color': 'hsl(335, 70%, 50%)',
          'x': 'KW',
          'y': 36
        },
        {
          'color': 'hsl(172, 70%, 50%)',
          'x': 'MK',
          'y': 10
        },
        {
          'color': 'hsl(144, 70%, 50%)',
          'x': 'NG',
          'y': 7
        },
        {
          'color': 'hsl(118, 70%, 50%)',
          'x': 'FJ',
          'y': 17
        },
        {
          'color': 'hsl(54, 70%, 50%)',
          'x': 'TM',
          'y': 12
        },
        {
          'color': 'hsl(51, 70%, 50%)',
          'x': 'TJ',
          'y': 12
        },
        {
          'color': 'hsl(227, 70%, 50%)',
          'x': 'PM',
          'y': 51
        },
        {
          'color': 'hsl(355, 70%, 50%)',
          'x': 'BA',
          'y': 44
        }
      ]
    },
    {
      'id': 'cognac',
      'color': 'hsl(296, 70%, 50%)',
      'data': [
        {
          'color': 'hsl(185, 70%, 50%)',
          'x': 'PT',
          'y': 7
        },
        {
          'color': 'hsl(308, 70%, 50%)',
          'x': 'KW',
          'y': 28
        },
        {
          'color': 'hsl(290, 70%, 50%)',
          'x': 'MK',
          'y': 42
        },
        {
          'color': 'hsl(325, 70%, 50%)',
          'x': 'NG',
          'y': 24
        },
        {
          'color': 'hsl(143, 70%, 50%)',
          'x': 'FJ',
          'y': 13
        },
        {
          'color': 'hsl(340, 70%, 50%)',
          'x': 'TM',
          'y': 21
        },
        {
          'color': 'hsl(306, 70%, 50%)',
          'x': 'TJ',
          'y': 47
        },
        {
          'color': 'hsl(140, 70%, 50%)',
          'x': 'PM',
          'y': 5
        },
        {
          'color': 'hsl(110, 70%, 50%)',
          'x': 'BA',
          'y': 59
        }
      ]
    }
  ],
  bar: [
    {
      'country': 'AD',
      'hot dog': 153,
      'hot dogColor': 'hsl(107, 70%, 50%)',
      'burger': 16,
      'burgerColor': 'hsl(310, 70%, 50%)',
      'sandwich': 66,
      'sandwichColor': 'hsl(101, 70%, 50%)',
      'kebab': 179,
      'kebabColor': 'hsl(214, 70%, 50%)',
      'fries': 77,
      'friesColor': 'hsl(321, 70%, 50%)',
      'donut': 56,
      'donutColor': 'hsl(227, 70%, 50%)'
    },
    {
      'country': 'AE',
      'hot dog': 57,
      'hot dogColor': 'hsl(164, 70%, 50%)',
      'burger': 22,
      'burgerColor': 'hsl(210, 70%, 50%)',
      'sandwich': 4,
      'sandwichColor': 'hsl(46, 70%, 50%)',
      'kebab': 93,
      'kebabColor': 'hsl(173, 70%, 50%)',
      'fries': 190,
      'friesColor': 'hsl(350, 70%, 50%)',
      'donut': 3,
      'donutColor': 'hsl(162, 70%, 50%)'
    },
    {
      'country': 'AF',
      'hot dog': 158,
      'hot dogColor': 'hsl(62, 70%, 50%)',
      'burger': 31,
      'burgerColor': 'hsl(144, 70%, 50%)',
      'sandwich': 174,
      'sandwichColor': 'hsl(227, 70%, 50%)',
      'kebab': 166,
      'kebabColor': 'hsl(226, 70%, 50%)',
      'fries': 106,
      'friesColor': 'hsl(95, 70%, 50%)',
      'donut': 83,
      'donutColor': 'hsl(206, 70%, 50%)'
    },
    {
      'country': 'AG',
      'hot dog': 168,
      'hot dogColor': 'hsl(25, 70%, 50%)',
      'burger': 160,
      'burgerColor': 'hsl(189, 70%, 50%)',
      'sandwich': 174,
      'sandwichColor': 'hsl(10, 70%, 50%)',
      'kebab': 6,
      'kebabColor': 'hsl(338, 70%, 50%)',
      'fries': 157,
      'friesColor': 'hsl(200, 70%, 50%)',
      'donut': 82,
      'donutColor': 'hsl(91, 70%, 50%)'
    },
    {
      'country': 'AI',
      'hot dog': 198,
      'hot dogColor': 'hsl(198, 70%, 50%)',
      'burger': 164,
      'burgerColor': 'hsl(338, 70%, 50%)',
      'sandwich': 49,
      'sandwichColor': 'hsl(323, 70%, 50%)',
      'kebab': 91,
      'kebabColor': 'hsl(60, 70%, 50%)',
      'fries': 155,
      'friesColor': 'hsl(194, 70%, 50%)',
      'donut': 147,
      'donutColor': 'hsl(283, 70%, 50%)'
    },
    {
      'country': 'AL',
      'hot dog': 79,
      'hot dogColor': 'hsl(129, 70%, 50%)',
      'burger': 160,
      'burgerColor': 'hsl(35, 70%, 50%)',
      'sandwich': 193,
      'sandwichColor': 'hsl(252, 70%, 50%)',
      'kebab': 102,
      'kebabColor': 'hsl(246, 70%, 50%)',
      'fries': 37,
      'friesColor': 'hsl(301, 70%, 50%)',
      'donut': 137,
      'donutColor': 'hsl(219, 70%, 50%)'
    },
    {
      'country': 'AM',
      'hot dog': 72,
      'hot dogColor': 'hsl(43, 70%, 50%)',
      'burger': 62,
      'burgerColor': 'hsl(126, 70%, 50%)',
      'sandwich': 10,
      'sandwichColor': 'hsl(287, 70%, 50%)',
      'kebab': 28,
      'kebabColor': 'hsl(95, 70%, 50%)',
      'fries': 161,
      'friesColor': 'hsl(224, 70%, 50%)',
      'donut': 122,
      'donutColor': 'hsl(30, 70%, 50%)'
    }
  ],
  calendar: [
  {
    "day": "2016-06-19",
    "value": 396
  },
  {
    "day": "2016-06-16",
    "value": 231
  },
  {
    "day": "2016-07-12",
    "value": 75
  },
  {
    "day": "2016-02-21",
    "value": 219
  },
  {
    "day": "2016-01-15",
    "value": 129
  },
  {
    "day": "2015-05-15",
    "value": 150
  },
  {
    "day": "2016-06-22",
    "value": 80
  },
  {
    "day": "2015-10-31",
    "value": 279
  },
  {
    "day": "2016-06-11",
    "value": 343
  },
  {
    "day": "2016-01-03",
    "value": 265
  },
  {
    "day": "2016-08-07",
    "value": 124
  },
  {
    "day": "2016-02-07",
    "value": 136
  },
  {
    "day": "2016-07-27",
    "value": 287
  },
  {
    "day": "2016-07-17",
    "value": 134
  },
  {
    "day": "2015-06-28",
    "value": 71
  },
  {
    "day": "2015-04-20",
    "value": 103
  },
  {
    "day": "2016-04-20",
    "value": 288
  },
  {
    "day": "2016-06-26",
    "value": 14
  },
  {
    "day": "2016-07-22",
    "value": 378
  },
  {
    "day": "2015-10-18",
    "value": 197
  },
  {
    "day": "2015-07-10",
    "value": 250
  },
  {
    "day": "2015-07-08",
    "value": 152
  },
  {
    "day": "2016-03-20",
    "value": 287
  },
  {
    "day": "2015-08-08",
    "value": 340
  },
  {
    "day": "2016-02-01",
    "value": 231
  },
  {
    "day": "2015-08-23",
    "value": 306
  },
  {
    "day": "2015-09-24",
    "value": 167
  },
  {
    "day": "2015-07-01",
    "value": 88
  },
  {
    "day": "2016-01-21",
    "value": 395
  },
  {
    "day": "2016-03-16",
    "value": 34
  },
  {
    "day": "2015-06-05",
    "value": 236
  },
  {
    "day": "2015-09-23",
    "value": 176
  },
  {
    "day": "2015-12-13",
    "value": 357
  },
  {
    "day": "2016-03-14",
    "value": 78
  },
  {
    "day": "2015-08-04",
    "value": 302
  },
  {
    "day": "2015-07-22",
    "value": 274
  },
  {
    "day": "2016-06-07",
    "value": 349
  },
  {
    "day": "2016-06-21",
    "value": 288
  },
  {
    "day": "2016-03-08",
    "value": 124
  },
  {
    "day": "2015-07-16",
    "value": 122
  },
  {
    "day": "2015-06-20",
    "value": 7
  },
  {
    "day": "2015-11-04",
    "value": 3
  },
  {
    "day": "2015-10-21",
    "value": 284
  },
  {
    "day": "2016-05-12",
    "value": 145
  },
  {
    "day": "2016-06-25",
    "value": 120
  },
  {
    "day": "2015-09-14",
    "value": 289
  },
  {
    "day": "2015-11-09",
    "value": 7
  },
  {
    "day": "2016-05-01",
    "value": 14
  },
  {
    "day": "2015-11-12",
    "value": 95
  },
  {
    "day": "2016-07-20",
    "value": 263
  },
  {
    "day": "2016-03-09",
    "value": 122
  },
  {
    "day": "2016-07-19",
    "value": 45
  },
  {
    "day": "2015-10-17",
    "value": 61
  },
  {
    "day": "2015-12-10",
    "value": 354
  },
  {
    "day": "2015-09-02",
    "value": 206
  },
  {
    "day": "2015-11-02",
    "value": 133
  },
  {
    "day": "2015-06-08",
    "value": 398
  },
  {
    "day": "2016-01-10",
    "value": 169
  },
  {
    "day": "2015-06-09",
    "value": 325
  },
  {
    "day": "2016-04-07",
    "value": 102
  },
  {
    "day": "2015-10-24",
    "value": 253
  },
  {
    "day": "2016-04-21",
    "value": 93
  },
  {
    "day": "2015-11-27",
    "value": 348
  },
  {
    "day": "2016-07-26",
    "value": 50
  },
  {
    "day": "2016-01-22",
    "value": 160
  },
  {
    "day": "2016-03-29",
    "value": 115
  },
  {
    "day": "2015-08-17",
    "value": 231
  },
  {
    "day": "2015-09-08",
    "value": 14
  },
  {
    "day": "2016-04-04",
    "value": 197
  },
  {
    "day": "2015-07-18",
    "value": 134
  },
  {
    "day": "2015-09-10",
    "value": 243
  },
  {
    "day": "2016-05-20",
    "value": 154
  },
  {
    "day": "2015-11-17",
    "value": 160
  },
  {
    "day": "2016-05-21",
    "value": 228
  },
  {
    "day": "2016-03-27",
    "value": 168
  },
  {
    "day": "2015-10-13",
    "value": 287
  },
  {
    "day": "2016-06-08",
    "value": 325
  },
  {
    "day": "2016-02-27",
    "value": 136
  },
  {
    "day": "2016-04-02",
    "value": 244
  },
  {
    "day": "2016-03-13",
    "value": 0
  },
  {
    "day": "2015-04-10",
    "value": 81
  },
  {
    "day": "2016-05-07",
    "value": 281
  },
  {
    "day": "2016-05-16",
    "value": 386
  },
  {
    "day": "2015-12-16",
    "value": 173
  },
  {
    "day": "2016-03-17",
    "value": 73
  },
  {
    "day": "2016-02-28",
    "value": 167
  },
  {
    "day": "2016-08-02",
    "value": 118
  },
  {
    "day": "2016-06-27",
    "value": 400
  },
  {
    "day": "2015-08-21",
    "value": 16
  },
  {
    "day": "2015-11-14",
    "value": 321
  },
  {
    "day": "2015-05-06",
    "value": 109
  },
  {
    "day": "2016-05-23",
    "value": 248
  },
  {
    "day": "2015-05-18",
    "value": 198
  },
  {
    "day": "2016-06-12",
    "value": 223
  },
  {
    "day": "2016-01-25",
    "value": 375
  },
  {
    "day": "2016-02-10",
    "value": 206
  },
  {
    "day": "2015-09-03",
    "value": 379
  },
  {
    "day": "2016-03-07",
    "value": 107
  },
  {
    "day": "2016-02-06",
    "value": 261
  },
  {
    "day": "2016-06-17",
    "value": 238
  },
  {
    "day": "2015-07-17",
    "value": 355
  },
  {
    "day": "2015-07-30",
    "value": 228
  },
  {
    "day": "2015-05-07",
    "value": 200
  },
  {
    "day": "2016-03-12",
    "value": 385
  },
  {
    "day": "2015-11-03",
    "value": 310
  },
  {
    "day": "2016-02-08",
    "value": 190
  },
  {
    "day": "2016-07-09",
    "value": 3
  },
  {
    "day": "2015-11-01",
    "value": 8
  },
  {
    "day": "2015-06-23",
    "value": 47
  },
  {
    "day": "2016-03-18",
    "value": 262
  },
  {
    "day": "2016-02-15",
    "value": 285
  },
  {
    "day": "2015-11-26",
    "value": 237
  },
  {
    "day": "2015-05-05",
    "value": 159
  },
  {
    "day": "2015-08-27",
    "value": 258
  },
  {
    "day": "2016-06-23",
    "value": 125
  },
  {
    "day": "2015-08-15",
    "value": 73
  },
  {
    "day": "2016-05-14",
    "value": 65
  },
  {
    "day": "2016-05-17",
    "value": 185
  },
  {
    "day": "2016-04-15",
    "value": 395
  },
  {
    "day": "2015-08-11",
    "value": 151
  },
  {
    "day": "2016-03-02",
    "value": 266
  },
  {
    "day": "2015-04-16",
    "value": 44
  },
  {
    "day": "2015-12-29",
    "value": 186
  },
  {
    "day": "2015-07-05",
    "value": 383
  },
  {
    "day": "2016-04-24",
    "value": 79
  },
  {
    "day": "2015-12-25",
    "value": 279
  },
  {
    "day": "2015-06-13",
    "value": 194
  },
  {
    "day": "2015-04-05",
    "value": 397
  },
  {
    "day": "2016-01-09",
    "value": 346
  },
  {
    "day": "2015-09-28",
    "value": 330
  },
  {
    "day": "2016-05-30",
    "value": 309
  },
  {
    "day": "2016-07-06",
    "value": 395
  },
  {
    "day": "2015-12-08",
    "value": 87
  },
  {
    "day": "2016-05-27",
    "value": 262
  },
  {
    "day": "2015-04-07",
    "value": 101
  },
  {
    "day": "2015-09-05",
    "value": 142
  },
  {
    "day": "2015-12-27",
    "value": 380
  },
  {
    "day": "2015-12-19",
    "value": 113
  },
  {
    "day": "2016-01-07",
    "value": 71
  },
  {
    "day": "2016-04-17",
    "value": 47
  },
  {
    "day": "2015-05-24",
    "value": 260
  },
  {
    "day": "2015-09-19",
    "value": 4
  },
  {
    "day": "2015-12-28",
    "value": 75
  },
  {
    "day": "2016-08-05",
    "value": 101
  },
  {
    "day": "2016-02-02",
    "value": 215
  },
  {
    "day": "2015-08-26",
    "value": 361
  },
  {
    "day": "2015-08-31",
    "value": 375
  },
  {
    "day": "2015-10-08",
    "value": 216
  },
  {
    "day": "2015-11-23",
    "value": 86
  },
  {
    "day": "2016-07-11",
    "value": 167
  },
  {
    "day": "2015-11-16",
    "value": 110
  },
  {
    "day": "2016-04-09",
    "value": 180
  },
  {
    "day": "2016-03-21",
    "value": 352
  },
  {
    "day": "2015-04-28",
    "value": 39
  },
  {
    "day": "2015-07-25",
    "value": 332
  },
  {
    "day": "2015-08-22",
    "value": 327
  },
  {
    "day": "2016-02-11",
    "value": 246
  },
  {
    "day": "2016-01-02",
    "value": 123
  },
  {
    "day": "2015-04-08",
    "value": 299
  },
  {
    "day": "2015-09-26",
    "value": 235
  },
  {
    "day": "2016-03-05",
    "value": 98
  },
  {
    "day": "2016-07-13",
    "value": 305
  },
  {
    "day": "2015-11-29",
    "value": 24
  },
  {
    "day": "2015-09-09",
    "value": 211
  },
  {
    "day": "2015-06-24",
    "value": 96
  },
  {
    "day": "2015-04-11",
    "value": 287
  },
  {
    "day": "2015-10-09",
    "value": 270
  },
  {
    "day": "2016-01-31",
    "value": 369
  },
  {
    "day": "2015-05-29",
    "value": 59
  },
  {
    "day": "2016-01-30",
    "value": 6
  },
  {
    "day": "2016-01-05",
    "value": 268
  },
  {
    "day": "2016-03-31",
    "value": 320
  },
  {
    "day": "2016-04-23",
    "value": 23
  },
  {
    "day": "2016-05-31",
    "value": 214
  },
  {
    "day": "2015-12-20",
    "value": 31
  },
  {
    "day": "2016-03-25",
    "value": 85
  },
  {
    "day": "2015-06-21",
    "value": 217
  },
  {
    "day": "2015-10-01",
    "value": 109
  },
  {
    "day": "2016-01-27",
    "value": 46
  },
  {
    "day": "2016-02-09",
    "value": 273
  },
  {
    "day": "2016-08-03",
    "value": 188
  },
  {
    "day": "2016-05-18",
    "value": 52
  },
  {
    "day": "2015-04-21",
    "value": 123
  },
  {
    "day": "2016-05-26",
    "value": 270
  },
  {
    "day": "2015-10-30",
    "value": 142
  },
  {
    "day": "2015-08-05",
    "value": 201
  },
  {
    "day": "2016-01-08",
    "value": 132
  },
  {
    "day": "2015-11-28",
    "value": 254
  },
  {
    "day": "2015-05-11",
    "value": 368
  },
  {
    "day": "2015-06-12",
    "value": 88
  },
  {
    "day": "2016-06-13",
    "value": 318
  },
  {
    "day": "2016-02-03",
    "value": 114
  },
  {
    "day": "2016-01-29",
    "value": 349
  },
  {
    "day": "2015-10-29",
    "value": 179
  },
  {
    "day": "2015-10-20",
    "value": 372
  },
  {
    "day": "2015-10-04",
    "value": 108
  },
  {
    "day": "2015-04-15",
    "value": 152
  },
  {
    "day": "2015-04-29",
    "value": 317
  },
  {
    "day": "2015-06-25",
    "value": 146
  },
  {
    "day": "2015-09-06",
    "value": 211
  },
  {
    "day": "2015-12-23",
    "value": 135
  },
  {
    "day": "2016-03-06",
    "value": 199
  },
  {
    "day": "2016-05-19",
    "value": 172
  },
  {
    "day": "2015-11-05",
    "value": 370
  },
  {
    "day": "2016-06-09",
    "value": 290
  },
  {
    "day": "2015-04-18",
    "value": 132
  },
  {
    "day": "2015-05-13",
    "value": 14
  },
  {
    "day": "2016-05-28",
    "value": 164
  },
  {
    "day": "2015-11-07",
    "value": 297
  },
  {
    "day": "2015-06-07",
    "value": 326
  },
  {
    "day": "2015-11-11",
    "value": 207
  },
  {
    "day": "2016-08-08",
    "value": 47
  },
  {
    "day": "2015-11-06",
    "value": 56
  },
  {
    "day": "2015-09-20",
    "value": 348
  },
  {
    "day": "2015-10-26",
    "value": 368
  },
  {
    "day": "2015-12-12",
    "value": 50
  },
  {
    "day": "2016-03-01",
    "value": 216
  },
  {
    "day": "2015-10-27",
    "value": 100
  },
  {
    "day": "2016-07-07",
    "value": 327
  },
  {
    "day": "2015-06-29",
    "value": 176
  },
  {
    "day": "2015-07-14",
    "value": 195
  },
  {
    "day": "2016-05-15",
    "value": 32
  },
  {
    "day": "2015-05-04",
    "value": 31
  },
  {
    "day": "2015-07-02",
    "value": 384
  },
  {
    "day": "2015-07-29",
    "value": 263
  },
  {
    "day": "2016-01-11",
    "value": 136
  },
  {
    "day": "2015-12-26",
    "value": 341
  },
  {
    "day": "2016-01-01",
    "value": 34
  },
  {
    "day": "2015-08-14",
    "value": 372
  },
  {
    "day": "2015-05-31",
    "value": 341
  },
  {
    "day": "2016-07-29",
    "value": 57
  },
  {
    "day": "2016-05-10",
    "value": 236
  },
  {
    "day": "2015-07-21",
    "value": 129
  },
  {
    "day": "2015-07-12",
    "value": 255
  },
  {
    "day": "2016-06-02",
    "value": 137
  },
  {
    "day": "2015-09-16",
    "value": 191
  },
  {
    "day": "2016-02-22",
    "value": 223
  },
  {
    "day": "2016-07-10",
    "value": 197
  },
  {
    "day": "2016-08-06",
    "value": 287
  },
  {
    "day": "2015-12-06",
    "value": 101
  },
  {
    "day": "2016-06-05",
    "value": 13
  },
  {
    "day": "2016-04-08",
    "value": 228
  },
  {
    "day": "2015-09-04",
    "value": 235
  },
  {
    "day": "2016-04-13",
    "value": 293
  },
  {
    "day": "2015-05-08",
    "value": 23
  },
  {
    "day": "2015-07-26",
    "value": 281
  },
  {
    "day": "2015-12-17",
    "value": 47
  },
  {
    "day": "2016-07-01",
    "value": 305
  },
  {
    "day": "2016-02-04",
    "value": 132
  },
  {
    "day": "2016-01-13",
    "value": 290
  },
  {
    "day": "2015-05-01",
    "value": 219
  },
  {
    "day": "2015-05-03",
    "value": 242
  },
  {
    "day": "2016-06-28",
    "value": 79
  },
  {
    "day": "2015-10-15",
    "value": 286
  },
  {
    "day": "2016-03-26",
    "value": 55
  },
  {
    "day": "2016-07-25",
    "value": 232
  },
  {
    "day": "2016-04-18",
    "value": 279
  },
  {
    "day": "2016-01-12",
    "value": 27
  },
  {
    "day": "2015-04-13",
    "value": 244
  },
  {
    "day": "2016-06-15",
    "value": 120
  },
  {
    "day": "2015-08-20",
    "value": 339
  },
  {
    "day": "2015-12-11",
    "value": 354
  },
  {
    "day": "2016-04-27",
    "value": 304
  },
  {
    "day": "2015-04-04",
    "value": 268
  },
  {
    "day": "2016-07-31",
    "value": 66
  },
  {
    "day": "2016-01-23",
    "value": 395
  },
  {
    "day": "2015-10-22",
    "value": 373
  },
  {
    "day": "2016-04-01",
    "value": 354
  },
  {
    "day": "2015-08-06",
    "value": 313
  },
  {
    "day": "2015-11-15",
    "value": 196
  },
  {
    "day": "2015-12-03",
    "value": 308
  },
  {
    "day": "2016-07-24",
    "value": 150
  },
  {
    "day": "2015-11-13",
    "value": 34
  },
  {
    "day": "2016-03-10",
    "value": 234
  },
  {
    "day": "2015-12-01",
    "value": 326
  },
  {
    "day": "2015-04-27",
    "value": 397
  },
  {
    "day": "2016-04-30",
    "value": 199
  }
],
radar: [
  {
    "taste": "fruity",
    "chardonay": 54,
    "carmenere": 93,
    "syrah": 46
  },
  {
    "taste": "bitter",
    "chardonay": 108,
    "carmenere": 64,
    "syrah": 46
  },
  {
    "taste": "heavy",
    "chardonay": 51,
    "carmenere": 21,
    "syrah": 50
  },
  {
    "taste": "strong",
    "chardonay": 62,
    "carmenere": 61,
    "syrah": 117
  },
  {
    "taste": "sunny",
    "chardonay": 79,
    "carmenere": 26,
    "syrah": 20
  }
],
treemap: {
  "name": "nivo",
  "color": "hsl(42, 70%, 50%)",
  "children": [
    {
      "name": "viz",
      "color": "hsl(298, 70%, 50%)",
      "children": [
        {
          "name": "stack",
          "color": "hsl(120, 70%, 50%)",
          "children": [
            {
              "name": "chart",
              "color": "hsl(225, 70%, 50%)",
              "loc": 149624
            },
            {
              "name": "xAxis",
              "color": "hsl(233, 70%, 50%)",
              "loc": 84932
            },
            {
              "name": "yAxis",
              "color": "hsl(232, 70%, 50%)",
              "loc": 141510
            },
            {
              "name": "layers",
              "color": "hsl(296, 70%, 50%)",
              "loc": 158887
            }
          ]
        },
        {
          "name": "pie",
          "color": "hsl(10, 70%, 50%)",
          "children": [
            {
              "name": "chart",
              "color": "hsl(131, 70%, 50%)",
              "children": [
                {
                  "name": "pie",
                  "color": "hsl(293, 70%, 50%)",
                  "children": [
                    {
                      "name": "outline",
                      "color": "hsl(174, 70%, 50%)",
                      "loc": 166166
                    },
                    {
                      "name": "slices",
                      "color": "hsl(130, 70%, 50%)",
                      "loc": 89146
                    },
                    {
                      "name": "bbox",
                      "color": "hsl(235, 70%, 50%)",
                      "loc": 85121
                    }
                  ]
                },
                {
                  "name": "donut",
                  "color": "hsl(83, 70%, 50%)",
                  "loc": 112838
                },
                {
                  "name": "gauge",
                  "color": "hsl(327, 70%, 50%)",
                  "loc": 155488
                }
              ]
            },
            {
              "name": "legends",
              "color": "hsl(268, 70%, 50%)",
              "loc": 198019
            }
          ]
        }
      ]
    },
    {
      "name": "colors",
      "color": "hsl(57, 70%, 50%)",
      "children": [
        {
          "name": "rgb",
          "color": "hsl(205, 70%, 50%)",
          "loc": 170714
        },
        {
          "name": "hsl",
          "color": "hsl(78, 70%, 50%)",
          "loc": 131346
        }
      ]
    },
    {
      "name": "utils",
      "color": "hsl(80, 70%, 50%)",
      "children": [
        {
          "name": "randomize",
          "color": "hsl(268, 70%, 50%)",
          "loc": 14055
        },
        {
          "name": "resetClock",
          "color": "hsl(273, 70%, 50%)",
          "loc": 119173
        },
        {
          "name": "noop",
          "color": "hsl(110, 70%, 50%)",
          "loc": 76797
        },
        {
          "name": "tick",
          "color": "hsl(246, 70%, 50%)",
          "loc": 11946
        },
        {
          "name": "forceGC",
          "color": "hsl(167, 70%, 50%)",
          "loc": 124204
        },
        {
          "name": "stackTrace",
          "color": "hsl(285, 70%, 50%)",
          "loc": 113612
        },
        {
          "name": "dbg",
          "color": "hsl(249, 70%, 50%)",
          "loc": 45670
        }
      ]
    },
    {
      "name": "generators",
      "color": "hsl(147, 70%, 50%)",
      "children": [
        {
          "name": "address",
          "color": "hsl(57, 70%, 50%)",
          "loc": 48093
        },
        {
          "name": "city",
          "color": "hsl(79, 70%, 50%)",
          "loc": 4962
        },
        {
          "name": "animal",
          "color": "hsl(262, 70%, 50%)",
          "loc": 89760
        },
        {
          "name": "movie",
          "color": "hsl(70, 70%, 50%)",
          "loc": 39287
        },
        {
          "name": "user",
          "color": "hsl(256, 70%, 50%)",
          "loc": 121905
        }
      ]
    },
    {
      "name": "set",
      "color": "hsl(263, 70%, 50%)",
      "children": [
        {
          "name": "clone",
          "color": "hsl(257, 70%, 50%)",
          "loc": 23615
        },
        {
          "name": "intersect",
          "color": "hsl(206, 70%, 50%)",
          "loc": 151927
        },
        {
          "name": "merge",
          "color": "hsl(154, 70%, 50%)",
          "loc": 174358
        },
        {
          "name": "reverse",
          "color": "hsl(180, 70%, 50%)",
          "loc": 166926
        },
        {
          "name": "toArray",
          "color": "hsl(236, 70%, 50%)",
          "loc": 30706
        },
        {
          "name": "toObject",
          "color": "hsl(112, 70%, 50%)",
          "loc": 175224
        },
        {
          "name": "fromCSV",
          "color": "hsl(348, 70%, 50%)",
          "loc": 177646
        },
        {
          "name": "slice",
          "color": "hsl(37, 70%, 50%)",
          "loc": 49103
        },
        {
          "name": "append",
          "color": "hsl(51, 70%, 50%)",
          "loc": 14531
        },
        {
          "name": "prepend",
          "color": "hsl(314, 70%, 50%)",
          "loc": 161482
        },
        {
          "name": "shuffle",
          "color": "hsl(239, 70%, 50%)",
          "loc": 196872
        },
        {
          "name": "pick",
          "color": "hsl(78, 70%, 50%)",
          "loc": 111108
        },
        {
          "name": "plouc",
          "color": "hsl(7, 70%, 50%)",
          "loc": 152599
        }
      ]
    },
    {
      "name": "text",
      "color": "hsl(129, 70%, 50%)",
      "children": [
        {
          "name": "trim",
          "color": "hsl(165, 70%, 50%)",
          "loc": 155206
        },
        {
          "name": "slugify",
          "color": "hsl(174, 70%, 50%)",
          "loc": 152923
        },
        {
          "name": "snakeCase",
          "color": "hsl(53, 70%, 50%)",
          "loc": 68932
        },
        {
          "name": "camelCase",
          "color": "hsl(268, 70%, 50%)",
          "loc": 126198
        },
        {
          "name": "repeat",
          "color": "hsl(357, 70%, 50%)",
          "loc": 62854
        },
        {
          "name": "padLeft",
          "color": "hsl(73, 70%, 50%)",
          "loc": 192034
        },
        {
          "name": "padRight",
          "color": "hsl(114, 70%, 50%)",
          "loc": 10556
        },
        {
          "name": "sanitize",
          "color": "hsl(235, 70%, 50%)",
          "loc": 81331
        },
        {
          "name": "ploucify",
          "color": "hsl(261, 70%, 50%)",
          "loc": 168818
        }
      ]
    },
    {
      "name": "misc",
      "color": "hsl(300, 70%, 50%)",
      "children": [
        {
          "name": "whatever",
          "color": "hsl(126, 70%, 50%)",
          "children": [
            {
              "name": "hey",
              "color": "hsl(322, 70%, 50%)",
              "loc": 1211
            },
            {
              "name": "WTF",
              "color": "hsl(14, 70%, 50%)",
              "loc": 178620
            },
            {
              "name": "lol",
              "color": "hsl(329, 70%, 50%)",
              "loc": 75212
            },
            {
              "name": "IMHO",
              "color": "hsl(103, 70%, 50%)",
              "loc": 159900
            }
          ]
        },
        {
          "name": "other",
          "color": "hsl(222, 70%, 50%)",
          "loc": 85660
        },
        {
          "name": "crap",
          "color": "hsl(218, 70%, 50%)",
          "children": [
            {
              "name": "crapA",
              "color": "hsl(113, 70%, 50%)",
              "loc": 69777
            },
            {
              "name": "crapB",
              "color": "hsl(90, 70%, 50%)",
              "children": [
                {
                  "name": "crapB1",
                  "color": "hsl(9, 70%, 50%)",
                  "loc": 65276
                },
                {
                  "name": "crapB2",
                  "color": "hsl(15, 70%, 50%)",
                  "loc": 158175
                },
                {
                  "name": "crapB3",
                  "color": "hsl(169, 70%, 50%)",
                  "loc": 100635
                },
                {
                  "name": "crapB4",
                  "color": "hsl(199, 70%, 50%)",
                  "loc": 180846
                }
              ]
            },
            {
              "name": "crapC",
              "color": "hsl(350, 70%, 50%)",
              "children": [
                {
                  "name": "crapC1",
                  "color": "hsl(211, 70%, 50%)",
                  "loc": 69080
                },
                {
                  "name": "crapC2",
                  "color": "hsl(359, 70%, 50%)",
                  "loc": 172784
                },
                {
                  "name": "crapC3",
                  "color": "hsl(341, 70%, 50%)",
                  "loc": 109199
                },
                {
                  "name": "crapC4",
                  "color": "hsl(337, 70%, 50%)",
                  "loc": 160851
                },
                {
                  "name": "crapC5",
                  "color": "hsl(80, 70%, 50%)",
                  "loc": 196315
                },
                {
                  "name": "crapC6",
                  "color": "hsl(146, 70%, 50%)",
                  "loc": 86490
                },
                {
                  "name": "crapC7",
                  "color": "hsl(306, 70%, 50%)",
                  "loc": 182045
                },
                {
                  "name": "crapC8",
                  "color": "hsl(324, 70%, 50%)",
                  "loc": 74264
                },
                {
                  "name": "crapC9",
                  "color": "hsl(122, 70%, 50%)",
                  "loc": 65130
                }
              ]
            }
          ]
        }
      ]
    }
  ]
}
}
